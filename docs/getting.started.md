<center>
# **Getting Started**
##### How to clone the repository and get started the right way with GitHub
</center>

---

## Cloning repository
- To clone the project, you'll need to request access to the repository first. One of the contacts who can grant this access is:<br>
1\. Richard Soares.
    
- To clone the repository from GitHub, you need the repository's URL. Open the terminal (or Git Bash on Windows) and use the following command.

Command to clone repository:

    git clone https://github.com/RickSRS/OpenAuth.git

## Navigate to the repository directory
- Use the **cd** command to navigate to the cloned repository's directory.

Example

    cd directory-repository-name

## Check existing branches
- To check the existing branches in the repository and see which branch you are currently on.

Command: 

    git branch

*The asterisk (\*) indicates the current branch.*

## Create a new branch
- To create a new branch, use the **git checkout -b** command followed by the new branch's name.

Example

    git checkout -b branch-name

## Make your changes
- Now that you are on your new branch, make the necessary changes to your project's files.

## Add and commit your changes
- After making your changes, add the modified files to the commit and commit the changes<br>

Commands:

    git add .
    git commit -m "Commit message

*Replace "Commit message" with a descriptive message of what was changed.*

## Push your branch to the remote repository
- To push your new branch and associated commits to the remote repository (GitHub).

Command

    git push origin branch-name

*This will push your new branch and its commits to the remote repository on GitHub.*

## Reasons for creating a branch
- **Isolation of changes:** Creating a branch allows you to work on new features or bug fixes without interfering with the main code (usually in the **main** or **master** branch). This prevents untested or incomplete changes from directly affecting the production code.
- **Ease of collaboration:** With separate branches, different team members can work on different parts of the project simultaneously, without interfering with each other's work. This facilitates collaboration and parallel development.
- **Version control:** Branches make version control easier, allowing you to work on different versions of your code and maintain a clear history of each change made.
- **Ease of reverting:** If something goes wrong with the changes in a branch, it's easier to revert to a previous version, as the changes are isolated in the specific branch.

Creating branches is a common practice in collaborative software development, as it helps keep the code organized, secure, and facilitates teamwork